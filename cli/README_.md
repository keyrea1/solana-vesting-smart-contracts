# Testing

Before start the testing `solana` cli-tools and `spl-token` must be installed.  
[How To Install Solana Tool Suite](https://docs.solana.com/cli/install-solana-cli-tools)  
[Install SPL-Token CLI-Utility](https://spl.solana.com/token#command-line-utility)

Create participants accounts:
```bash
solana-keygen new --outfile ~/.config/solana/id_owner.json --force
solana-keygen new --outfile ~/.config/solana/id_dest.json --force
solana-keygen new --outfile ~/.config/solana/id_new_dest.json --force
```

Owner would do all operations, so put some SOL to his account:
```bash
solana airdrop 10 ~/.config/solana/id_owner.json
```

Deploy program and copy PROGRAM_ID.  
Must be run from the `program` folder:
```bash
cd ./program
cargo build-bpf
cd -
solana deploy ./program/target/deploy/token_vesting.so -k ~/.config/solana/id_owner.json
```
Since the output of the deployment-command above is not only the Program ID, set environment variable `PROGRAM_ID` manually:  
```bash
PROGRAM_ID=...
```

Next, set the owner's keypair as default:
```bash
solana config set -k ~/.config/solana/id_owner.json
```

Create vesting destination token account(ACCOUNT_TOKEN_DEST):
```bash
spl-token create-account $MINT ~/.config/solana/id_dest.json
```

And new one(ACCOUNT_TOKEN_NEW_DEST):
```bash
spl-token create-account $MINT ~/.config/solana/id_new_dest.json
```

Build CLI:

```bash
cargo build
```

Create vesting instance and store its `SEED` value.  
Pay attention: values of arguments `--start-time` and `--release-time` are the UNIX-Timestamp of moments when the vesting starts and ands respectively. 
```bash
echo "RUST_BACKTRACE=1 ./target/debug/vesting-contract-cli                          \
--url $(solana config get json_rpc_url | cut -d ' ' -f 3)                                                     \
--program_id $PROGRAM_ID                           \
create                                                                              \
--mint_address $MINT                         \
--source_owner ~/.config/solana/id_owner.json                                             \
--source_token_address $TOKEN_ACCOUNT                                             \
--destination_token_address $ACCOUNT_TOKEN_DEST                  \
--amount 10000000000                                                                   \
--start-time 1637317800 \
--release-time 1637321400 \
--payer ~/.config/solana/id_owner.json"                  \
--verbose | bash              
```

To use [Associated Token Account](https://spl.solana.com/associated-token-account) as destination use `--destination_address`(with public key of `id_dest`) instead of `--destination_token_address`.

Observe contract state:
```bash
echo "RUST_BACKTRACE=1 ./target/debug/vesting-contract-cli                          \
--url $(solana config get json_rpc_url | cut -d ' ' -f 3)                                                     \
--program_id $PROGRAM_ID                           \
info                                                                                \
--seed $SEED " | bash                                          
```

Change owner:
```bash
echo "RUST_BACKTRACE=1 ./target/debug/vesting-contract-cli                          \
--url $(solana config get json_rpc_url | cut -d ' ' -f 3)                                                     \
--program_id $PROGRAM_ID                           \
change-destination                                                                  \
--seed $SEED                                  \
--current_destination_owner ~/.config/solana/id_dest.json                           \
--new_destination_token_address $ACCOUNT_TOKEN_NEW_DEST        \
--payer ~/.config/solana/id_owner.json" | bash                           
```

And unlock tokens according schedule:
```bash
echo "RUST_BACKTRACE=1 ./target/debug/vesting-contract-cli                          \
--url $(solana config get json_rpc_url | cut -d ' ' -f 3)                                                     \
--program_id $PROGRAM_ID                           \
unlock                                                                              \
--seed $SEED                                  \
--payer ~/.config/solana/id_owner.json" | bash
```

## Links

https://spl.solana.com/token
