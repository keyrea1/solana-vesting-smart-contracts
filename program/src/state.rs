use solana_program::{
    program_error::ProgramError,
    program_pack::{IsInitialized, Pack, Sealed},
    pubkey::Pubkey,
};

use std::convert::TryInto;

#[derive(Debug, PartialEq)]
pub struct VestingSchedule {
    pub release_time: u64,
    pub amount: u64,
}

#[derive(Debug, PartialEq)]
pub struct VestingHeader {
    pub destination_address: Pubkey,
    pub mint_address: Pubkey,
    pub start_time: u64,
    pub release_time: u64,
    pub amount: u64,
    pub withdrawn: u64,
    pub is_initialized: bool,
}

impl Sealed for VestingHeader {}

impl Pack for VestingHeader {
    const LEN: usize = 97;

    fn pack_into_slice(&self, target: &mut [u8]) {
        let destination_address_bytes = self.destination_address.to_bytes();
        let mint_address_bytes = self.mint_address.to_bytes();
        for i in 0..32 {
            target[i] = destination_address_bytes[i];
        }

        for i in 32..64 {
            target[i] = mint_address_bytes[i - 32];
        }

        let start_time_bytes = self.start_time.to_le_bytes();
        let release_time_bytes = self.release_time.to_le_bytes();
        let amount_bytes = self.amount.to_le_bytes();
        let withdrawn_bytes = self.withdrawn.to_le_bytes();
        for i in 64..72 {
            target[i] = start_time_bytes[i - 64];
        }

        for i in 72..80 {
            target[i] = release_time_bytes[i - 72];
        }

        for i in 80..88 {
            target[i] = amount_bytes[i - 80];
        }

        for i in 88..96 {
            target[i] = withdrawn_bytes[i - 88];
        }

        target[96] = self.is_initialized as u8;
    }

    fn unpack_from_slice(src: &[u8]) -> Result<Self, ProgramError> {
        if src.len() < VestingHeader::LEN {
            return Err(ProgramError::InvalidAccountData);
        }
        let destination_address = Pubkey::new(&src[..32]);
        let mint_address = Pubkey::new(&src[32..64]);
        let start_time = u64::from_le_bytes(src[64..72].try_into().unwrap());
        let release_time = u64::from_le_bytes(src[72..80].try_into().unwrap());
        let amount = u64::from_le_bytes(src[80..88].try_into().unwrap());
        let withdrawn = u64::from_le_bytes(src[88..96].try_into().unwrap());
        let is_initialized = src[96] == 1;
        Ok(Self {
            destination_address,
            mint_address,
            start_time,
            release_time,
            amount,
            withdrawn,
            is_initialized,
        })
    }
}

impl IsInitialized for VestingHeader {
    fn is_initialized(&self) -> bool {
        self.is_initialized
    }
}

#[cfg(test)]
mod tests {
    use super::VestingHeader;
    use solana_program::{program_pack::Pack, pubkey::Pubkey};

    #[test]
    fn test_state_packing() {
        let header_state = VestingHeader {
            destination_address: Pubkey::new_unique(),
            mint_address: Pubkey::new_unique(),
            start_time: 30567976,
            release_time: 30767976,
            amount: 969,
            withdrawn: 0,
            is_initialized: true,
        };
        let state_size = VestingHeader::LEN;
        let mut state_array = [0u8; 97];
        header_state.pack_into_slice(&mut state_array[..VestingHeader::LEN]);
        let packed = Vec::from(state_array);
        let mut expected = Vec::with_capacity(state_size);
        expected.extend_from_slice(&header_state.destination_address.to_bytes());
        expected.extend_from_slice(&header_state.mint_address.to_bytes());
        expected.extend_from_slice(&header_state.start_time.to_le_bytes());
        expected.extend_from_slice(&header_state.release_time.to_le_bytes());
        expected.extend_from_slice(&header_state.amount.to_le_bytes());
        expected.extend_from_slice(&header_state.withdrawn.to_le_bytes());
        expected.extend_from_slice(&[header_state.is_initialized as u8]);

        assert_eq!(expected, packed);
        assert_eq!(packed.len(), state_size);
        let unpacked_header =
            VestingHeader::unpack(&packed[..VestingHeader::LEN]).unwrap();
        assert_eq!(unpacked_header, header_state);
    }
}