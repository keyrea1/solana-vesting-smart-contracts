import { PublicKey } from '@solana/web3.js';
import { Numberu64 } from './utils';

// export class Schedule {
//   // Release time in unix timestamp
//   releaseTime!: Numberu64;
//   amount!: Numberu64;
//
//   constructor(releaseTime: Numberu64, amount: Numberu64) {
//     this.releaseTime = releaseTime;
//     this.amount = amount;
//   }
//
//   public toBuffer(): Buffer {
//     return Buffer.concat([this.releaseTime.toBuffer(), this.amount.toBuffer()]);
//   }
//
//   static fromBuffer(buf: Buffer): Schedule {
//     const releaseTime: Numberu64 = Numberu64.fromBuffer(buf.slice(0, 8));
//     const amount: Numberu64 = Numberu64.fromBuffer(buf.slice(8, 16));
//     return new Schedule(releaseTime, amount);
//   }
// }

export class VestingHeader {
  destinationAddress!: PublicKey;
  mintAddress!: PublicKey;
  isInitialized!: boolean;
  startTime!: Numberu64;
  releaseTime!: Numberu64;
  amount!: Numberu64;
  withdrawn!: Numberu64;

  constructor(
    destinationAddress: PublicKey,
    mintAddress: PublicKey,
    isInitialized: boolean,
    startTime: Numberu64,
    releaseTime: Numberu64,
    amount: Numberu64,
    withdrawn: Numberu64,
  ) {
    this.destinationAddress = destinationAddress;
    this.mintAddress = mintAddress;
    this.isInitialized = isInitialized;
    this.startTime = startTime;
    this.releaseTime = releaseTime;
    this.withdrawn = withdrawn;
  }

  static fromBuffer(buf: Buffer): VestingHeader {
    const destinationAddress = new PublicKey(buf.slice(0, 32));
    const mintAddress = new PublicKey(buf.slice(32, 64));
    const startTime = Numberu64.fromBuffer(buf.slice(64, 72));
    const releaseTime = Numberu64.fromBuffer(buf.slice(72, 80));
    const amount = Numberu64.fromBuffer(buf.slice(80, 88));
    const withdrawn = Numberu64.fromBuffer(buf.slice(88, 96));
    const isInitialized = buf[96] == 1;
    const header: VestingHeader = {
      destinationAddress,
      mintAddress,
      isInitialized,
      startTime,
      releaseTime,
      amount,
      withdrawn,
    };
    return header;
  }
}

export class ContractInfo {
  destinationAddress!: PublicKey;
  mintAddress!: PublicKey;
  startTime!: Numberu64;
  releaseTime!: Numberu64;
  amount!: Numberu64;
  withdrawn!: Numberu64;

  constructor(
    destinationAddress: PublicKey,
    mintAddress: PublicKey,
    startTime: Numberu64,
    releaseTime: Numberu64,
    amount: Numberu64,
    withdrawn: Numberu64,
  ) {
    this.destinationAddress = destinationAddress;
    this.mintAddress = mintAddress;
    this.startTime = startTime;
    this.releaseTime = releaseTime;
    this.amount = amount;
    this.withdrawn =  withdrawn;
  }

  static fromBuffer(buf: Buffer): ContractInfo | undefined {
    const header = VestingHeader.fromBuffer(buf);
    if (!header.isInitialized) {
      return undefined;
    }
    // const schedules: Array<Schedule> = [];
    // for (let i = 65; i < buf.length; i += 16) {
    //   schedules.push(Schedule.fromBuffer(buf.slice(i, i + 16)));
    // }
    return new ContractInfo(
      header.destinationAddress,
      header.mintAddress,
      header.startTime,
      header.releaseTime,
      header.amount,
      header.withdrawn
    );
  }
}
